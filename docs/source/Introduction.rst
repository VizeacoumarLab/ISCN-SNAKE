Purpose of ISCNSNAKE
===========================

.. _Introduction

The Mitelman Database of Chromosome Aberrations and Gene Fusions in Cancer is a resource that contains over 70,000 karyotypes from tumour samples. The format it is written in, ISCN (International System for Cytogenetic Nomenclature), has been difficult to computationally analyze. This python module will give researchers the tools they need to analyze the quantitative aspects of ISCN data.

What ISCNSNAKE can do:
++++++++++++++++++++++++++
* Analyzes quantitative abnormalities in ISCN format.
* Output ISCN data into a format ready for visualization with circos.
* Analyze frequency of quantitative and non-quantitative aberrations.
* Analyze coincidence of gains/losses across different regions.


What ISCNSNAKE does NOT do:
++++++++++++++++++++++++++++++++
* Analyze breakpoints of non-quantitative aberrations in ISCN format.
    * If the aberration does not cause variation in gene number, it will not be analyzed by ISCNSNAKE (as an example, balanced translocations are not added. If I get demand for the analysis of non-quantitative aberrations this could be added in a future update.
* Analyze coincidence of aberrations at the aberration level.  
