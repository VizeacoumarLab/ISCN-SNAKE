Using filters to select subsets of the Mitelman Database
===========================================================
.. _Filters

Currently you are able to filter the Mitelman Database by eight different parameters:::
Reference Number
Case Number
Investigation Number
Author, Year
Journal Name
Volume, Page
Morphology
Topography

All of those can be used, though most users will be primarily concerned with the last two of these. To use filters successfully it must be inputted exactly as in the Mitelman database (as they were previously written). The attributes you wish to select from the database must also be correctly typed exactly as the appear in the database. At the bottom of this page is a list of all possible attributes for Morphology and Topography as they appear in the database.

Filter syntax is "column:attribute". For example, if you wanted to select only cases with topography indicated as breast, you would enter "Topography:Breast" as a filter. If you want to add additional filters, such as wanting to specify only adenocarcinomas with the same topography, create a second filter with the same syntax and separate it with two forward slashes. Example: "Topography:Breast//Morphology:Adenocarcinoma". Double slashes are used as commas, spaces, and dashes are all within the attributes in the Mitelman Database.

Multiple filters of the same column can be applied, however filters indicate allowable values so if you want to analyze lung adenocarcinomas as well as all breast tumour categories, you will have to run the program twice as only breast will be analyzed.

quick_SNAKE Filtering
++++++++++++++++++++++

In quick_SNAKE filters are not given separated by double forward slashes. Enter each filter seperately and then press enter to input.