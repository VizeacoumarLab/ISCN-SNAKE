Visualization with Circos
=======================================
.. _Visualization

ISCNSNAKE is designed to work with circos, so it is recommended that you read their documentation at `Link text <circos.ca/documentation/>`_ if you have any in depth questions. This tutorial will show how to make a very simple histogram out of the data.

It is very easy to create simple visualizations out of the results of your ISCNSNAKE analysis. Just add the filenames in your circos.conf file and run circos. Only parameters that you need to specify in the conf are type (histogram), colours to use and axes minimum and maximum.
