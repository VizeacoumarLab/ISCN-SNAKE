Running ISCNSNAKE
=====================

.. _ISCNSNAKE

The full version of the program can be ran with the following prompt:

``python -m ISCNSNAKE.run_SNAKE [filename optional]``

This will open a menu that will allow you to configure many options. Here is a brief description of what each of the options does. Most options are self descriptive, such as options for filenames. To change the name of the file or the final path of the folder, it is as easy as pressing the desired number and typing in the new path. This supports standard unix path options. 

run_SNAKE is a very simple script, that only acts as a call to ISCNParser.parse_File, which has it's own menu. If you were analyzing many data samples and didn't want to use the menu each time, calling parse_file from another script allows for options to be automated (using skipmenu = True runs the parser with the kwargs that you specify).

* 1 -- input filename
    * Required. The path to a plain text version of the Mitelman Database of Chromosome Abnormalities OR the datafile you are working with. If using Mitelman data, make sure the first line of the file is the column titles. In some versions you will have a line at the start of the file that says *Content: plain-text*, you will have to remove this to analyze properly. The other format of data that is accepted is ISCN data, as in a file with only ISCN entries in it. It should also have no blank lines or even headers
    * ex: ../../Mitelman.txt or ISCN_example.txt
* 2 -- input data type
    * Can be set to either ISCN or Mitelman. Default ISCN. Mitelman is the format that the text version of the database is in, as provided in Mitelman.txt. ISCN is a format where each karyotype in ISCN format is on a seperate line by itself, as in ISCN_example.txt
* 3 -- use consensus karyotype 
    * Set this to one the 3 following options:
        * first_only: only analyzes the first clone in each ISCN, good for small data sets with few clonal karyotypes and one or two massive karyotypes with 10+ clones
        * seperate: analyzes each clone as a seperate patient, your frequency percentages will be very low, I personally don't recommend this option, but I can see some cases where you might want to do in depth analysis of a single karyotypewith comparision to the clonal types.
        * merge: creates a consensus karyotype out of all the clonal karyotypes. Best for large generalized datasets such as samples from the Mitelman Database.
* 4 -- output to file 
    * If this is False, you will get no output. Can be good if you just want the raw data. To use this you will have to change how you call run_SNAKE or assign _ to a variable afterwards.
* 6 -- output mode
    * relative if you want percentages
    * minmax if you want it to be a result between 0 and 1 (for machine learning and possibly other purposes)
* 7 -- current filters
    * Enter filters as described for quick_SNAKE (see filters documentation) but instead of seperating with double slashes just enter each one seperately then press enter. 
* 15 -- analyze dependancies
    * Will make a list of co occurances of various aberrations. Very very very very slow (factorial time). Creates a list of links in circos format. Only use this for very few samples as comparision of each cytogenetic band to every other band is exponential with regards to number of aberrations and linear with regards to number of samples. 
* 16 -- dependence quantile
    * Trims links to the quantile selected. Useful as most dependancy matrixes end up with hundreds of thousands of co-occurances.
* 18 -- analyze errors
    * Set to True if you want a seperate file containing every instance of an unparsable karyotype. Could be used pretty easily to check your own karyotypes to check for proper ISCN syntax. Will try to explain errors but isn't very descriptive.
* 20 -- legacy ploidy correction
    * This mode allows correction for ploidy to revert to the pre 2.3 operation of the ISCNSNAKE. When True the program will convert all deletions that are homozygous to -2 and all heterozygous deletions to -1. If your concern is more related to questions regarding homozygosity vs heterozygosity, enable this, but recognize your raw ISCN file will not contain absolute loss values. Example: ISCN 88,XX,-2,-2,-2,-2. In this ISCN we have a karyotype that is tetraploid and has lost all four copies of chromosome 2. If this feature is disabled (default behaviour) the rawISCN file will show a value of -4 for the chromosome 2 region, but if this were enabled it would only show -2 (indicating a homozygous deletion. On the other side, with the karyotype 67,XX,-2,-2,-2, where only 3 copies are lost, the default behaviour will return  the absolute value (-3) and the legacy ploidy correction behavious will only indicate -1. This mode essentially changes the definition of deep loss, once enabled, deep losses are synonymous with homozygous deletions, but when turned off (default) the definition is changed so that all deletions of 2 or more copies is marked as a deep deletion. There is some ambiguity in the level of ploidy in any given sample so this ends up being somewhat of an estimate of homozygous deletions, and is sometimes prone to false positives and negatives,
* 21 -- verbose
    * Set to True if you want all the karyotypes the program reads to be printed to console. (same as -v in quick_SNAKE)
